
functionn [y , t ] = forwardEuler(@lotka , t0 ,T , y0 , N )
 %Solve dy/dt = f(t,y) , y(t0 😞 y0
  h = ( T - t0 )/( N -1); % Calulate and store the step - size
   t = linspace( 0 ,0.0125 , 2 ); % A vector to store the time values .
    y0 = [0.5; 1; 2]; 
     y = zeros (1 , N ); % Initialize the Y vector .
      y (1) = y0 ; % Start y at the initial value .
       for i = 1:( N -1)
	        y (i +1)= y(i)+ h*f( t(i) , y(i)); % Update approximation y at t+h
		 end
