function [error] = direct3( )

Guess = [0.0038; 0.9545];

[p, error] = fminsearch(@direct2, Guess);

days = 0:12;
S = [762 740 650 400 250 120 80 50 20 18 15 13 10];
I = [1 20 80 220 300 260 240 190 120 80 20 5 2];

[t, y] = ode23(@direct1, [0,12], [S(1); I(1)],[], p);

subplot(2,1,1);
plot(t, y(:,1), days, S, 'o')
title('Susceptible population','FontSize',11)

subplot(2,1,2);
plot(t, y(:,2), days, I, 'o')
title('Infected population','FontSize',11)
