function p = populations1(p,t)
p = p(2)*p(3)./((p(2)-p(3))*exp(-p(1)*t)+p(3));
end
