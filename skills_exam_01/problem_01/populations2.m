function error=populations2(p)


decades=0:10:150;
pops =[3.929 5.308 7.24 9.638 12.866 17.069 23.192 31.443 38.558 50.156 62.948 75.996 91.972 105.711 122.775 131.669];

%pops=[3.93 5.31 7.24 9.64 12.87 17.07 23.19 31.44 38.558 50.16 62.95 75.99 91.97 105.71 122.78 131.67];
po = [.01; 1000; 3.93];
[p,error] = lsqcurvefit(@populations1,po,decades,pops)
sqrt_error=sqrt(error)

modelpops=populations1(p,decades);
plot(decades,pops,'o',decades,modelpops,'*')

end