function  error=populations3(po)

optimset lsqcurvefit
lowerbound=[0 0 0];
options=optimset('TolFun',1e-8);

decades=0:10:150;
pops =[3.929 5.308 7.24 9.638 12.866 17.069 23.192 31.443 38.558 50.156 62.948 75.996 91.972 105.711 122.775 131.669];

po = [.01; 1000; 3.93];

[p, error]=lsqcurvefit(@populations1,po,decades,pops,lowerbound,[],options)

modelpops=populations1(p,decades);
plot(decades,pops,'o',decades,modelpops)
end
